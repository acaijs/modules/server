// Packages
import * as path	from "path";
import { route }	from "@acai/router";
import test			from "@acai/testing";

// Utils
import request	from "./utils/request";

// Modules
import Server from "../index";

test.group("Server body request tests", (group) => {
	// general instance to be reused and dispatched
	let server: Server;

	// make sure no server instances are kept running after each test
	group.afterEach(async () => { if (server) await server.stop() });
	group.afterEach(route.clear);

	// -------------------------------------------------
	// JSON
	// -------------------------------------------------

	test.group("JSON requests", () => {
		test("correctly get fields from post", async (expect) => {
			let fields;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.post("/").send({ field1: true, field2: "test" });
	
			expect(type).toBe("application/json");
			expect(fields).toBe({ field1: true, field2: "test" });
		});
	
		test("correctly get fields from patch", async (expect) => {
			let fields;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.post("/").send({ field1: true, field2: "test" });
	
			expect(type).toBe("application/json");
			expect(fields).toBe({ field1: true, field2: "test" });
		});
	
		test("correctly get fields from put", async (expect) => {
			let fields;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.post("/").send({ field1: true, field2: "test" });
	
			expect(type).toBe("application/json");
			expect(fields).toBe({ field1: true, field2: "test" });
		});
	});

	// -------------------------------------------------
	// FormData
	// -------------------------------------------------
	
	test.group("FormData requests", () => {
		test("correctly get fields from post", async (expect) => {
			let fields;
			let files;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				files = request.files;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.post("/")
				.field({ field1: "true", field2: "test" })
				.attach("file", path.join(process.cwd(), "src/tests/utils/file.txt"));
	
			expect(fields).toBe({ field1: "true", field2: "test" });
			expect(files).toBeDefined();
			expect(type).toContain("multipart/form-data");
		});
		
		test("correctly get fields from patch", async (expect) => {
			let fields;
			let files;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				files = request.files;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.patch("/")
				.field({ field1: "true", field2: "test" })
				.attach("file", path.join(process.cwd(), "src/tests/utils/file.txt"));
	
			expect(fields).toBe({ field1: "true", field2: "test" });
			expect(files).toBeDefined();
			expect(type).toContain("multipart/form-data");
		});
		
		test("correctly get fields from put", async (expect) => {
			let fields;
			let files;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				files = request.files;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.put("/")
				.field({ field1: "true", field2: "test" })
				.attach("file", path.join(process.cwd(), "src/tests/utils/file.txt"));
	
			expect(fields).toBe({ field1: "true", field2: "test" });
			expect(files).toBeDefined();
			expect(type).toContain("multipart/form-data");
		});
	});

	// -------------------------------------------------
	// URL encoded
	// -------------------------------------------------

	test.group("URL encoded requests", () => {
		test("correctly get fields from post", async (expect) => {
			let fields;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.post("/").type("form").send({ field1: true, field2: "test" });
	
			expect(fields).toBe({ field1: "true", field2: "test" });
			expect(type).toBe("application/x-www-form-urlencoded");
		});
	
		test("correctly get fields from patch", async (expect) => {
			let fields;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.post("/").type("form").send({ field1: true, field2: "test" });
	
			expect(fields).toBe({ field1: "true", field2: "test" });
			expect(type).toBe("application/x-www-form-urlencoded");
		});
	
		test("correctly get fields from put", async (expect) => {
			let fields;
			let type;
			server = new Server();
	
			// create routes
			route("/", "src/tests/utils/response.exception@test");
	
			server.addGlobal((request) => {
				type = request.headers["content-type"];
				fields = request.fields;
				return request.fields;
			});
	
			await server.run();
	
			// make request
			await request.post("/").type("form").send({ field1: true, field2: "test" });
	
			expect(fields).toBe({ field1: "true", field2: "test" });
			expect(type).toBe("application/x-www-form-urlencoded");
		});
	});
}).tag(["server"]);